<?php

namespace App\Http\Controllers;

use App\Shipping;
use App\User;
use App\Driver;
use Illuminate\Http\Request;
use App\Http\Requests\StoreShipping;
use App\Http\Requests\GetShipping;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ShippingController extends GoiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(GetShipping $request)
    {
        //
        try {
            
            $validated = $request->validated();

            $user = JWTAuth::parseToken()->toUser();
            /**
             * Search response for request in cache
             */
            $cacheName =  env('APP_ENV').__METHOD__.$user->id.$request->input('start').$request->input('end');
            $data = $this->isCached($cacheName);
            
            if (!$data) {
                $data = Shipping::byDriver($user->id, $request->input('start'), $request->input('end'));
                
                /**
                 * Save response for 60 minutes
                 */
                $this->putCache($data, $cacheName);
            }

            
            return response()->json($data, 200);

        } catch (\Exception $e) {
            return $this->responseWrapper(false, false, $e, 400);
        } 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreShipping $request)
    {
        $validated = $request->validated();

        $response = [];
        try {
            
            $shipping = new Shipping($request->input());
            $shipping->save();
            
            /**
             * Get user info from token
             */
            $userId = JWTAuth::parseToken()->toUser()->id;
            $user = User::findOrFail($userId);

            $driverId = Driver::random();

            
            /**
             * Store in pivot relation
             */
            $user->shippings()->attach($shipping->id,['driver_id'=> $driverId, 'status'=>'pending']);

            
            $response = $shipping->load('user')->load('driver');

            
            Cache::flush();
            return $this->responseWrapper(true, $response, null, 201);           


        } catch (\Exception $e) {
            return $this->responseWrapper(false, false, $e, 400);
        }  
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function show(Shipping $shipping)
    {
        //
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shipping $shipping)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shipping $shipping)
    {
        //
    }
}
