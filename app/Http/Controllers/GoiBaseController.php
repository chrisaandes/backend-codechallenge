<?php


namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Cache;

class GoiBaseController extends Controller
{

    public function responseWrapper($success, $response, $error = false, $httpCode = 200){
        if (!$success) {
			$response = [
				"error" => $error->getMessage(),
				"file" => $error->getFile(),
				"line" => $error->getLine()
            ];		

        }

        return response()->json($response, $httpCode);
    }


    public function isCached($cacheName){
        return $returnData = unserialize(Cache::get($cacheName));
    }


    public function putCache($data, $cacheName){
        $expiresAt = Carbon::now()->addMinutes(config('params.minsCache'));
        Cache::put($cacheName, serialize($data), $expiresAt);
    }


}
