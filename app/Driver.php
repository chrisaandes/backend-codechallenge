<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    /**
     *  Pivot relation
     */
    public function shippings(){
        return $this->belongsToMany('\App\Shipping','driver_shipping_user')
            ->withPivot('shipping_id','status');
    }

    public function users(){
        return $this->belongsToMany('\App\User','driver_shipping_user')
            ->withPivot('user_id','status');
    }

    /**
     * Relation with user data
     */

    public function user(){
        return $this->belongsTo('App\User');
    }

    
    /**
     * Local scope for get random driver
     */
    public function scopeRandom($query){
        return $query->select('id')->where('availability',1)->orderByRaw("RAND()")->first()->id;
    }
    
    
}
