<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['address', 'deliver_at', 'time', 'observations'];

    public function user(){
    	return $this->belongsToMany('\App\User','driver_shipping_user')
                ->withPivot('user_id','status'); 
    } 

    public function driver(){ 
        return $this->belongsToMany('\App\Driver','driver_shipping_user')->with('user')
                ->withPivot('driver_id','status'); 
    }

    /**
     * Local scope for get shippings for driver
     *
     * @var object     
     */
    public function scopeByDriver($query, $driverId, $start, $end){
        // return $this->driver()->wherePivot('driver_id', $driverId)->get();

        return $query->whereBetween('deliver_at', [$start, $end])->whereHas('driver', function ($q) use ($driverId) {
            $q->where('driver_id', $driverId);
        })->with('user')->paginate(15);
    }
}
