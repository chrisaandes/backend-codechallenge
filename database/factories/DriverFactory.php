<?php

use Faker\Generator as Faker;

$factory->define(App\Driver::class, function (Faker $faker) {
    return [
        'dni' => $faker->randomNumber($nbDigits = NULL, $strict = false),
        'vehicle' => $faker->words($nb = 3, $asText = true),
        'availability' => $faker->randomElement($array = array (0,1))
    ];
});
