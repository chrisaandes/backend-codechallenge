<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        factory(App\User::class, 10)->create()->each(function($user) {
            factory(App\Driver::class, 1)->create(['user_id' => $user->id]);
        });


        factory(App\User::class, 10)->create();

        factory(App\Shipping::class, 10)->create()->each(function($shipping) {
            $statuses = ['pending', 'on route', 'delivered'];
            $randomKey = array_rand($statuses);
            $status = $statuses[$randomKey];


            \DB::table('driver_shipping_user')->insert(
                [
                    'shipping_id' => $shipping->id,
                    'status' => $status,
                    'user_id' => App\User::select('id')->orderByRaw("RAND()")->first()->id,
                    'driver_id' => App\Driver::select('id')->where('availability',1)->orderByRaw("RAND()")->first()->id,
                ]
            );
        });
    }
}
