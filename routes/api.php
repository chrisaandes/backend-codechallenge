<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/**
 * Auth Routes
 */
Route::group(['prefix' => 'v1/auth'], function () {
    Route::post('login', 'Auth\LoginController@login');
});

/**
 * Secure routes
 */
Route::group([
    
        'middleware' => 'auth',
        'prefix' => 'v1'
    
    ], function ($router) {
    
        Route::apiResource('shippings', 'ShippingController');
});


/**
 * Clear cache route
 */

Route::get('/clear-cache', function () {
    Cache::flush();
});

